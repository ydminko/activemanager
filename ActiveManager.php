<?php

namespace ydminko\activeManager;

use Yii;
use yii\base\Component;
use yii\base\Event;
use yii\base\InvalidParamException;

class ActiveManager extends Component
{
    /**
     * @var string
     */
    public $queryClass = 'yii\db\ActiveQuery';
    /**
     * [
     *      'User' => 'common\models\User',
     *      'user\Profile' => 'common\models\UserProfile'
     * ]
     * @var array
     */
    public $entityMap = [];
    /**
     * [
     *      'User' => [
     *          'profile' => ['user\Profile', ['id' => 'id'], 'andWhere' => ['email' => null]]
     *      ],
     *      'user\Profile' => [
     *          'user' => ['User', ['id' => 'id']]
     *      ],
     * ]
     * @var array
     */
    public $relationMap = [];
    /**
     * [
     *      'User' => 'common\db\ActiveQuery',
     *      'user\Profile' => ['class' => 'yii\db\ActiveQuery', 'where' => ['status' => 'active']],
     * ]
     * @var array
     */
    public $queryMap = [];
    /**
     * [
     *      'User' => [
     *          ['afterSave', 'common\models\Notify::handler'],
     *      ],
     * ]
     * @var array
     */
    public $events = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerHandlers();
    }

    /**
     * @param string $modelClass
     * @return \yii\db\ActiveQueryInterface|\yii\db\ActiveQuery
     */
    public function getQuery($modelClass)
    {
        $entity = $this->getEntity($modelClass);
        $config = []; $class = $this->queryClass;
        if (isset($this->queryMap[$entity])) {
            $query = $this->queryMap[$entity];
            if (is_string($query)) {
                $class = $query;
            } elseif (isset($query['class'])) {
                $class = $query['class'];
                unset($query['class']);
                $config = $query;
            }
        }
        $query = Yii::createObject($class, [$modelClass]);

        return $this->configureQuery($query, $config);
    }

    /**
     * @param \yii\db\ActiveRecord $model
     * @param string $name
     * @param boolean $throwException
     * @return null|\yii\db\ActiveQueryInterface
     */
    public function getRelation($model, $name, $throwException = true)
    {
        $relation = $this->createRelation($model, $name);
        if ($relation === null && $throwException) {
            throw new InvalidParamException('Relation not exist.');
        }
        return $relation;
    }

    /**
     * @param \yii\db\ActiveRecordInterface $model
     * @param string $name
     * @return boolean
     */
    public function hasRelation($model, $name)
    {
        $entity = $this->getEntity($model);
        return isset($this->relationMap[$entity]) && isset($this->relationMap[$entity][$name]);
    }

    /**
     * @param \yii\db\ActiveRecordInterface $model
     * @param string $name
     * @return null|\yii\db\ActiveQueryInterface
     */
    protected function createRelation($model, $name)
    {
        if (!$this->hasRelation($model, $name)) {
            return null;
        }
        $entity = $this->getEntity($model);
        $config = $this->relationMap[$entity][$name];
        $relation = $this->getQuery(array_shift($config));
        $relation->link = array_shift($config);
        $relation->multiple = array_shift($config);
        $relation->primaryModel = $model;

        return $this->configureQuery($relation, $config);
    }

    /**
     * @param string|object $alias
     * @return string
     */
    protected function getEntity($alias)
    {
        if (is_object($alias)) {
            $alias = get_class($alias);
        }
        $key = array_search($alias, $this->entityMap);
        return is_string($key) ? $key : $alias;
    }

    /**
     * @param \yii\db\ActiveQueryInterface $query
     * @param array $config
     * @throws InvalidParamException
     * @return \yii\db\ActiveQueryInterface
     */
    protected function configureQuery($query, $config)
    {
        foreach ($config as $func => $params) {
            if (!method_exists($query, $func)) {
                throw new InvalidParamException('Query config error.');
            }
            $query = call_user_func_array($query, $func, $params);
        }

        return $query;
    }

    /**
     *
     */
    protected function registerHandlers()
    {
        foreach ($this->events as $entity => $handlers) {
            $entity = $this->getEntity($entity);
            foreach ($handlers as $handler) {
                $name = array_shift($handler);
                $callback = array_shift($handler);
                $append = array_shift($handler) !== false;
                Event::on($entity, $name, $callback, $handler, $append);
            }
        }
    }
}