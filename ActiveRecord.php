<?php

namespace ydminko\activeManager;

use Yii;

class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @return ActiveManager
     */
    public static function getActiveManager()
    {
        return Yii::$app->get('activeManager');
    }
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return static::getActiveManager()->getQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getRelation($name, $throwException = true)
    {
        return static::getActiveManager()->getRelation($this, $name, $throwException);
    }
    public function hasRelation($name)
    {
        return static::getActiveManager()->hasRelation($this, $name);
    }
    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if ($this->hasRelation($name) && !$this->isRelationPopulated($name)) {
            $related = $this->getRelation($name)->findFor($name, $this);
            $this->populateRelation($name, $related);
            return $related;
        }
        return parent::__get($name);
    }
}